package ru.infoshell.utils.helpers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by Andrey Riyk
 */
public class ScreenSizeHelper {


    // TODO Wrong values on china devices (for example: Lenovo A880)
    public static float getScreenInches(Activity activity){
        DisplayMetrics dm = new DisplayMetrics();
        if(Build.VERSION.SDK_INT >= 17 )
            activity.getWindowManager().getDefaultDisplay().getRealMetrics(dm);
        else
            activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        double x = Math.pow(dm.widthPixels/dm.xdpi, 2);
        double y = Math.pow(dm.heightPixels/dm.ydpi, 2);
        double screenInches = Math.sqrt(x+y);
        return (float) screenInches;
    }



    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static int getWidth(Context context){
        int width=0;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        if(Build.VERSION.SDK_INT >= 17 ){
            Point size = new Point();
            display.getRealSize(size);
            width = size.x;
        }
        else if(Build.VERSION.SDK_INT >= 13 ){
            Point size = new Point();
            display.getSize(size);
            width = size.x;
        }
        else{
            width = display.getWidth();  // deprecated
        }
        return width;
    }



    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static int getHeight(Context context){
        int height=0;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        if(Build.VERSION.SDK_INT >= 17 ){
            Point size = new Point();
            display.getRealSize(size);
            height = size.y;
        }
        else if(Build.VERSION.SDK_INT >= 13 ){
            Point size = new Point();
            display.getSize(size);
            height = size.y;
        }else{
            height = display.getHeight();  // deprecated
        }
        return height;
    }

}

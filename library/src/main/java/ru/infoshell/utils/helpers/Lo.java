package ru.infoshell.utils.helpers;

import android.util.Log;

/**
 * Alternative to the standard Log.*
 *
 * <br><br>Created by Andrey Riyk
 */
public class Lo {

    private static String logTag = "my_app";

    private static boolean needLog = true;


    /**
     * Initialization, set tag for log
     */
    public static void init(String logTagName){
        logTag = logTagName;
    }


    /**
     * Setter for needLog
     */
    public static void setNeedLogs(boolean need){
        needLog = need;
    }


	public static void d(String s, String s2){Lo.d(s2); }

	public static void d(String s){
		if (needLog ) Log.d(logTag, s);
	}

	public static void v(String s, String s2){Lo.v(s2); }

	public static void v(String s){
		if (needLog ) Log.v(logTag, s);
	}

	public static void i(String s, String s2){Lo.i(s2); }

	public static void i(String s){
		if (needLog ) Log.i(logTag, s);
	}

	public static void e(String s, String s2){Lo.e(s2); }

	public static void e(String s){
		if (needLog ) Log.e(logTag, s);
	}

	public static void w(String s, String s2){Lo.w(s2); }

	public static void w(String s){
		if (needLog ) Log.w(logTag, s);
	}

	
}

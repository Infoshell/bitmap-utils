package ru.infoshell.utils.graphic;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;

/**
 * Helper for round bitmap
 *
 * <br><br>Created by Andrey Riyk
 */
public class BitmapRounder {


    /**
     * Round bitmap
     *
     * <br><br>Закругление углов изображения
     * @param bit - Изображение для закругления
     * @param partOfHeightToRound - Степень закругления ( от 0 до 0.5)
     * @return
     */
	public static Bitmap round(Bitmap bit, float partOfHeightToRound, Bitmap.Config bitmapConfig){

        if (bit==null || bit.isRecycled()) return null;

		final int sourceWidth = bit.getWidth();
		final int sourceHeight = bit.getHeight();
		
		final Bitmap output = Bitmap.createBitmap( sourceWidth, sourceHeight, bitmapConfig);

        final BitmapShader shader = new BitmapShader(bit, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

        final Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setShader(shader);

        final RectF rect = new RectF(0.0f, 0.0f, sourceWidth, sourceHeight);
        final Canvas canvas = new Canvas(output);
		canvas.drawRoundRect(rect, sourceHeight*partOfHeightToRound, sourceHeight*partOfHeightToRound, paint);

		return output;
	}
	
}

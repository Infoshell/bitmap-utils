package ru.infoshell.utils.graphic;

import android.graphics.Bitmap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import ru.infoshell.utils.helpers.Lo;

/**
 * Helper-class for saving bitmaps
 *
 * <br><br>Created by Andrey Riyk
 */
public class BitmapSave {


    /**
     * Save image to disk
     */
    public static boolean saveToDisk(Bitmap mBitmap, String filePath, Bitmap.CompressFormat compressFormat, int quality){
        try {
            File file2Save = new File(filePath);
            FileOutputStream fos = new FileOutputStream(file2Save);
            mBitmap.compress(compressFormat, quality, fos);
            fos.flush();
            fos.close();
        }catch (Exception e) {
            Lo.e("saveToDisk : " + e);
            e.printStackTrace();
            return false;
        }
        return true;
    }



    /**
     * Get time im mills, when file was modified, or 0 if file not exist
     */
    public static long getFileLastModify(String path){
        File file = new File(path);
        return file.exists()?  file.lastModified() : 0;
    }



    /**
     * Copy file with InputStream
     */
    public static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }

}

package ru.infoshell.utils.graphic;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;

/**
 * Helper for resizing bitmaps
 *
 * <br><br>Created by Andrey Riyk
 */
public class BitmapResizeHelper {



    /**
     * Check bitmap on disk and resize it with saving, if need
     */
    public static void checkImageSize(String path2File, int maxWidth, int maxHeight){
        final int rotation = BitmapRotation.getRotation(path2File);
        Bitmap ourImage = decodeSampledBitmapFromDisk(path2File, maxWidth , maxHeight, false);
        if (ourImage == null) return;
        ourImage = getScaledBitmap(ourImage, maxWidth, maxHeight);
        if (rotation !=0) ourImage = BitmapRotation.rotate(ourImage, rotation);
        BitmapSave.saveToDisk(ourImage, path2File, Bitmap.CompressFormat.JPEG, 85);
        ourImage.recycle();
    }



    /**
     * Get image from disk with resizing
     */
    public static Bitmap decodeSampledBitmapFromDisk(String path2File, int reqWidth, int reqHeight, boolean exactlySize) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path2File, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight, true);
        options.inJustDecodeBounds = false;
        return exactlySize?
                getScaledBitmap(BitmapFactory.decodeFile(path2File, options), reqWidth, reqHeight)
                : BitmapFactory.decodeFile(path2File, options);
    }



    /**
     * Calculating value of reducing image size
     *
     * <br><br>Просчет во сколько раз (степень 2) надо уменьшить изображение, что бы вписать его в необходимый максимальный размер
     * @param nearBig - если true то беребся ближайшее большее значение, иначе - ближайшее меньшее
     */
    private static int calculateInSampleSize( BitmapFactory.Options options, int reqWidth, int reqHeight, boolean nearBig) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        final int bigSide = height>width? height : width;
        int inSampleSize = 1;

        if (width > reqWidth) {
            // добавлено деление на 2 (bigSide/inSampleSize/2)
            // т.к. окончательная подгонка должна производиться от большего разрешения
            // , а не растягивать получившееся меньшее
            while(bigSide/inSampleSize/(nearBig? 2 : 1) > reqWidth){
                inSampleSize *=2;
            }
        }
        return inSampleSize;
    }



    /**
     * Get bitmap exactly (may bee -1) given size
     *
     * <br><br>Возвращает битмап точно (возможно округление int на -1) нужного размера.
     * <br> Относительно не экономный по памяти метод,
     * нужен для простого уменьшения изображения прямо в RAM, без промежуточных stream-ов
     * <br> Не рекомендуется использовать на больших изображениях.
     * Большие изображения необходимо брать с диска методом decodeSampledBitmapFromDisk, который сначала просчитывает степень ужатия.
     */
    public static Bitmap getScaledBitmap(Bitmap source, int maxW, int maxH) {
        if (source == null) return null;
        final int sourceW = source.getWidth();
        final int sourceH = source.getHeight();
        final int bigSideSource = sourceW> sourceH? sourceW : sourceH;
        final int bigSideReq = maxW> maxH? maxW : maxH;
        final float ratio = (float)bigSideSource / (float)bigSideReq;

        final int destW = (int)( (float)sourceW/ratio);
        final int destH = (int)( (float)sourceH/ratio);

        final Bitmap output = Bitmap.createBitmap(destW, destH, Bitmap.Config.ARGB_4444);
        final Rect destRect = new Rect(0, 0, destW, destH);
        Canvas canvas = new Canvas(output);
        canvas.drawBitmap(source, null, destRect, null);

        return output;
    }


}

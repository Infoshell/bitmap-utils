package ru.infoshell.utils.graphic;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Helper to write text on the image
 *
 * <br><br>Created by Andrey Riyk
 */
public class BitmapTextCreator {


	/**
	 * Write text on the image
	 */
	public static Bitmap applyText(Bitmap source, String text, int textSize, int x, int y, int color, Bitmap.Config bitmapConfig) {
		
		if (source == null) return null;

		final int sourceWidth = source.getWidth();
		final int sourceHeight = source.getHeight();

		final Bitmap output = Bitmap.createBitmap( (sourceWidth),  (sourceHeight), bitmapConfig);
		final Rect dest = new Rect(0, 0,  (sourceWidth), (sourceHeight));

		Canvas canvas = new Canvas(output);

		canvas.drawBitmap(source, null, dest, null);

		final Paint paint = new Paint();
		paint.setColor(color);
		paint.setTextSize(textSize);
		paint.setAntiAlias(true);

		canvas.drawText(text, x, y , paint );

		return output;
	}
	
}

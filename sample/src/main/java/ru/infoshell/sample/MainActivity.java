package ru.infoshell.sample;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ru.infoshell.utils.helpers.Lo;
import ru.infoshell.utils.helpers.ScreenSizeHelper;
import ru.infoshell.utils.helpers.TimerMeter;
import ru.infoshell.utils.graphic.BitmapCropper;
import ru.infoshell.utils.graphic.BitmapResizeHelper;
import ru.infoshell.utils.graphic.BitmapRounder;
import ru.infoshell.utils.graphic.BitmapSave;
import ru.infoshell.utils.graphic.BitmapTextCreator;


/**
 * Sample activity for images processing in another thread
 */
public class MainActivity extends ContextLeakTestActivity {

    private static ExecutorService executor = Executors.newSingleThreadExecutor();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ll1);
        final WeakReference<LinearLayout> weakLayout = new WeakReference<LinearLayout>(linearLayout);
        final WeakReference<Activity> weakActivity = new WeakReference<Activity>(this);

        Lo.i("MainActivity onCreate ScreenSizeHelper inches: "+ ScreenSizeHelper.getScreenInches(this));
        Lo.i("MainActivity onCreate ScreenSizeHelper w/h: "+ ScreenSizeHelper.getWidth(getApplicationContext())+ " / "+ScreenSizeHelper.getHeight(getApplicationContext()));


        // reduce image size
        executor.submit(new Runnable1(weakLayout, weakActivity));

        final String fileName5k = "five_thousand.jpg";
        final String fileName = getApplicationContext().getCacheDir() + "/" + fileName5k;
        // reduce big image from disk
        executor.submit(new Runnable2(weakLayout, weakActivity, fileName5k, fileName));

        // cropping and rounding image
        executor.submit(new Runnable3(weakLayout, weakActivity));

    }


    /** reduce image size (static class for avoid this.-memory leak in activity) */
    private static class Runnable1 implements Runnable {

        final WeakReference<LinearLayout> weakLayout;
        final WeakReference<Activity> weakActivity;

        private Runnable1(WeakReference<LinearLayout> weakLayout, WeakReference<Activity> weakActivity) {
            this.weakLayout = weakLayout;
            this.weakActivity = weakActivity;
        }

        @Override
        public void run() {
            int size = 50;
            Activity activity = weakActivity.get();
            if (activity == null) return;
            Bitmap bitmap = BitmapFactory.decodeResource(activity.getResources(), R.drawable.lenna);
            TimerMeter tm = new TimerMeter("timer - getScaledBitmap small");
            final Bitmap bitmapScaled = BitmapTextCreator.applyText(
                    BitmapResizeHelper.getScaledBitmap(bitmap, size, size), "size " + size, 12, 2, 14, Color.BLUE, Bitmap.Config.ARGB_8888);
            tm.end();
            final LinearLayout ll = weakLayout.get();
            if (ll != null)
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ImageView imageView = getImageView(ll, ll.getContext().getResources().getDimensionPixelSize(R.dimen.image_size));
                        imageView.setImageBitmap(bitmapScaled);
                    }
                });
        }
    }



    /** reduce big image from disk (static class for avoid this.-memory leak in activity) */
    private static class Runnable2 implements Runnable {

        final WeakReference<LinearLayout> weakLayout;
        final WeakReference<Activity> weakActivity;
        final String fileName5k, fileName;

        private Runnable2(WeakReference<LinearLayout> weakLayout, WeakReference<Activity> weakActivity, String fileName5k, String fileName) {
            this.weakLayout = weakLayout;
            this.weakActivity = weakActivity;
            this.fileName5k = fileName5k;
            this.fileName = fileName;
        }

        @Override
        public void run() {
            // copy big image to disk
            InputStream in = null;
            OutputStream out = null;
            try {
                Activity activity = weakActivity.get();
                if (activity == null) return;
                in = activity.getApplicationContext().getAssets().open(fileName5k);
                File outFile = new File(activity.getApplicationContext().getCacheDir(), fileName5k);
                out = new FileOutputStream(outFile);
                BitmapSave.copyFile(in, out);
                in.close();
                out.flush();
                out.close();
            } catch (IOException e) {
                Lo.e("Failed to copy asset file: " + fileName);
                e.printStackTrace();
            }
            // reduce image size when read from disk
            int size = 200;
            TimerMeter tm = new TimerMeter("timer 4 - decodeSampledBitmapFromDisk 5k");
            Bitmap bitmap = BitmapResizeHelper.decodeSampledBitmapFromDisk(fileName, size, size, false); // here we got OOM if use BitmapFactory.decodeFile(fileName);
            final Bitmap bitmapWithText = BitmapTextCreator.applyText(bitmap, "Decode sampled", 24, 40, 30, Color.BLUE, Bitmap.Config.ARGB_8888);
            tm.end();
            final LinearLayout ll = weakLayout.get();
            Activity activity = weakActivity.get();
            if (ll != null && activity!=null)
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ImageView imageView = getImageView(ll, ll.getContext().getResources().getDimensionPixelSize(R.dimen.image_size));
                        imageView.setImageBitmap(bitmapWithText);
                    }
                });
        }
    }



    /** cropping and rounding image (static class for avoid this.-memory leak in activity) */
    private static class Runnable3 implements Runnable {

        final WeakReference<LinearLayout> weakLayout;
        final WeakReference<Activity> weakActivity;

        private Runnable3(WeakReference<LinearLayout> weakLayout, WeakReference<Activity> weakActivity) {
            this.weakLayout = weakLayout;
            this.weakActivity = weakActivity;
        }

        @Override
        public void run() {
            int size = 300;
            TimerMeter tm = new TimerMeter("timer 6 - getScaledBitmap small with cropping and rounding");
            Activity activity = weakActivity.get();
            if (activity==null) return;
            Bitmap bitmap = BitmapFactory.decodeResource(activity.getApplicationContext().getResources(), R.drawable.lenna_long);
            activity = null; // for cases, when next we got long processing
            final Bitmap bitmapScaled = BitmapResizeHelper.getScaledBitmap(bitmap, size, size);
            tm.end();
            final LinearLayout ll = weakLayout.get();
            Activity activityAfterWork = weakActivity.get();
            if (ll!=null && activityAfterWork!=null)
                activityAfterWork.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ImageView imageView = getImageView(ll, ll.getContext().getResources().getDimensionPixelSize(R.dimen.image_size));
                        imageView.setImageBitmap(BitmapTextCreator.applyText(
                                BitmapRounder.round(
                                    BitmapCropper.getCroppedBitmapBox(bitmapScaled, Integer.MAX_VALUE), 0.2f, Bitmap.Config.ARGB_8888)
                                , "Rounded", 24, 40, 30, Color.BLUE, Bitmap.Config.ARGB_8888));
                    }
                });
        }
    }



    /** generate ImageView for given layout */
    private static ImageView getImageView(LinearLayout parent, int size){
        ImageView imageView = new ImageView(parent.getContext());
        parent.addView(imageView);
        imageView.getLayoutParams().height = size;
        imageView.getLayoutParams().width = size;
        ((LinearLayout.LayoutParams)imageView.getLayoutParams()).bottomMargin = 10;
        return imageView;
    }



    public void startLoadingActivity(View v){
        startActivity(new Intent(MainActivity.this, LoadingSampleActivity.class));
    }
}

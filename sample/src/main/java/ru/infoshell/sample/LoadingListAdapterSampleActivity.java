package ru.infoshell.sample;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import ru.infoshell.utils.graphic.BitmapRounder;
import ru.infoshell.utils.graphic.network.QueueImageLoader;


/**
 * Sample activity with BaseAdapter
 */
public class LoadingListAdapterSampleActivity extends ContextLeakTestActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_adapter_loading);

        getSupportActionBar().setTitle("ListView sample");

        ListView listView = (ListView) findViewById(R.id.listView);
        MyAdapter adapter = new MyAdapter(this, LoadingRecyclerViewSampleActivity.imagesUrl);
        listView.setAdapter(adapter);
    }



    /** Adapter for ListView */
    public class MyAdapter extends BaseAdapter {
        private List<String> items = new ArrayList<String>();
        private final LayoutInflater inflater;
        private WeakReference<Activity> weakActivity;

        public MyAdapter(Activity activity, List<String> items) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.items = items;
            weakActivity = new WeakReference<Activity>(activity);
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final ViewHolder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item, parent, false);
                holder = new ViewHolder((TextView) convertView.findViewById(R.id.textView), (ImageView) convertView.findViewById(R.id.imageView));
                convertView.setTag(holder);
            }
            else
                holder = (ViewHolder) convertView.getTag();

            holder.textView.setText(" "+getItem(position));
            holder.textView.setTextColor(Color.BLACK);

            holder.imageView.setImageDrawable(null);
            holder.imageView.clearAnimation();
            final long beginTime = System.currentTimeMillis();
            QueueImageLoader.justLoadInToImageView(weakActivity.get(), holder.imageView, items.get(position), 2, 0, new QueueImageLoader.CallbackWithImageView() {
                @Override
                public void onReady(String info, Bitmap bitmap,  WeakReference<ImageView> weakImageView) {
                    holder.textView.setText(holder.textView.getText()+"\n"+(System.currentTimeMillis() - beginTime)+ " ms.");
                    holder.textView.setTextColor(Color.BLUE);
                    ImageView imageView = weakImageView.get();
                    if (imageView!=null){
                        Animation animation = AnimationUtils.loadAnimation(imageView.getContext(), R.anim.anim_transparent_to_visible);
                        imageView.startAnimation(animation);
                    }
                }

                @Override
                public void onError(String info) {

                }
            },
            new AdditionalWorker()
            );

            return convertView;
        }

    }


    private static class ViewHolder{

        TextView textView;
        ImageView imageView;

        private ViewHolder(TextView textView, ImageView imageView) {
            this.textView = textView;
            this.imageView = imageView;
        }
    }



    /** static class for avoid this.-memory leak in activity */
    private static class AdditionalWorker implements QueueImageLoader.AdditionalWork{

        @Override
        public Bitmap work(Bitmap bitmap) {
            return BitmapRounder.round(bitmap, 0.15f, Bitmap.Config.ARGB_8888);
        }
    }

}

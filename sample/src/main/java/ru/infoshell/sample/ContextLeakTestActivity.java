package ru.infoshell.sample;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ListView;

/**
 * Created by Andrey Riyk
 */
public abstract class ContextLeakTestActivity extends ActionBarActivity {

    public static final boolean NEED_WORKLOAD = false;
    protected int workload = 4 * 1024 * 1024;
    protected byte[] workloadBytes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (NEED_WORKLOAD) {
            workloadBytes = new byte[workload];
            for (int i = 0; i < workload; i++) {
                workloadBytes[i] = 12;
            }
        }
    }
}

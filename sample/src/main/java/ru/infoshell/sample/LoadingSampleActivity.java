package ru.infoshell.sample;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.lang.ref.WeakReference;

import ru.infoshell.utils.graphic.BitmapCropper;
import ru.infoshell.utils.graphic.BitmapResizeHelper;
import ru.infoshell.utils.graphic.BitmapRounder;
import ru.infoshell.utils.graphic.BitmapTextCreator;
import ru.infoshell.utils.graphic.network.QueueImageLoader;
import ru.infoshell.utils.helpers.Lo;


/**
 * Sample activity
 */
public class LoadingSampleActivity extends ContextLeakTestActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_loading);
        final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ll1);

        loadingImageSimple(linearLayout);

        loadingImageWithCustomCallback(linearLayout);
    }



    /**
     * Simple image loading
     */
    private void loadingImageSimple(final LinearLayout linearLayout){
        final ImageView imageView2 = (ImageView) findViewById(R.id.imageView1);
        imageView2.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.image_size);
        imageView2.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.image_size);
        QueueImageLoader.justLoadInToImageView(this, imageView2, "http://img-fotki.yandex.ru/get/6503/119528728.21dc/0_fb7ed_44fef23c_XL", 0);
    }



    /**
     * Loading image with custom callback
     */
    private void loadingImageWithCustomCallback(final LinearLayout linearLayout){
        ImageView imageView2 = (ImageView) findViewById(R.id.imageView2);
        final WeakReference<ImageView> weakReference = new WeakReference<ImageView>(imageView2);
        QueueImageLoader.addTask("https://pp.vk.me/c624729/v624729941/a2d7/nCej3xoNEIY.jpg", new QueueImageLoader.ImageLoadingCallback() {
            @Override
            public void onReady(final String info, final Bitmap bitmap) {
                Lo.i("QueueImageLoader onReady ");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmapTmp =  BitmapResizeHelper.getScaledBitmap(bitmap, 200, 200);
                        bitmapTmp = BitmapCropper.getCroppedBitmapBox( bitmapTmp, Integer.MAX_VALUE);
                        bitmapTmp = BitmapRounder.round(bitmapTmp, 0.499999f, Bitmap.Config.ARGB_8888);
                        bitmapTmp = BitmapTextCreator.applyText(bitmapTmp, "From web", 24, 40, 30, Color.BLUE, Bitmap.Config.ARGB_8888);
                        ImageView imageView = weakReference.get();
                        if (imageView!=null)
                            imageView.setImageBitmap(bitmapTmp);
                    }
                });
            }

            @Override
            public void onError(final String info) {
                Lo.i("QueueImageLoader onError "+info);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoadingSampleActivity.this, "Error loading bitmap: " + info, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }, true, false, 5, 0, null);
    }


    public void startLoadingRecyclerActivity(View v){
        startActivity(new Intent(this, LoadingRecyclerViewSampleActivity.class));
    }

    public void startLoadingAdapterActivity(View v){
        startActivity(new Intent(this, LoadingListAdapterSampleActivity.class));
    }
}
